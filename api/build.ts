import { getAllMilestones } from "./gitlab/milestones";
import { getMergeRequestsFromMilestone } from "./gitlab/merge_requests";
import PaperCut from "./papercut";
import { OUTPUT_PATH } from "./constants";
import Bun from "bun";

fetchMilestones();

async function fetchMilestones() {
  const milestones = await getAllMilestones();
  let milestonesWithPapercuts = [];

  for (const milestone of milestones) {
    console.log("---------------------------------------");
    console.log("Merge requests for milestone: " + milestone.title);

    let mergeRequests = await getMergeRequestsFromMilestone(milestone.title);
    console.log(
      `Successfully fetched ${mergeRequests.length} Merge Requests for ${milestone.title}`
    );

    let papercuts = mergeRequests
      .map((mr) => PaperCut.fromMergeRequest(mr))
      .filter((paperCut) => paperCut !== null);

    console.log(
      `Generated ${papercuts.length} Papercuts for ${milestone.title}`
    );

    if (papercuts.length > 0) {
      milestonesWithPapercuts.push(milestone);

      await Bun.write(
        `${OUTPUT_PATH}/${milestone.title}.json`,
        JSON.stringify(papercuts)
      );
    }
  }

  await Bun.write(
    `${OUTPUT_PATH}/milestones.json`,
    JSON.stringify(milestonesWithPapercuts)
  );
}
