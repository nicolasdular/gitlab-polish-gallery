import { request, gql } from "graphql-request";
import { API_URL, LABELS } from "../constants";

export interface MergeRequest {
  id: string;
  title: string;
  username: string;
  avatarUrl: string;
  milestone: string;
  url: string;
  descriptionHtml: string;
  labels: string[];
}

interface PageInfo {
  hasNextPage: boolean;
  endCursor: string;
}

interface MergeRequestNode {
  id: string;
  mergedAt: string;
  webUrl: string;
  title: string;
  descriptionHtml: string;
  milestone: {
    title: string;
  };
  author: {
    username: string;
    avatarUrl: string;
  };
  labels: {
    nodes: {
      title: string;
    }[];
  };
}

interface MergeRequestQueryData {
  project: {
    mergeRequests: {
      pageInfo: PageInfo;
      nodes: MergeRequestNode[];
    };
  };
}

const QUERY = gql`
  query ($after: String!, $labels: [String!], $milestone: String!) {
    project(fullPath: "gitlab-org/gitlab") {
      mergeRequests(
        labels: $labels
        state: merged
        sort: MERGED_AT_DESC
        after: $after
        milestoneTitle: $milestone
      ) {
        pageInfo {
          hasNextPage
          endCursor
        }
        nodes {
          id
          mergedAt
          webUrl
          title
          descriptionHtml
          milestone {
            title
          }
          author {
            username
            avatarUrl
          }
          labels {
            nodes {
              title
            }
          }
        }
      }
    }
  }
`;

export async function getMergeRequestsFromMilestone(milestone: string) {
  const mergeRequests: MergeRequest[] = [];

  const labels = Object.keys(LABELS);

  for (const label of labels) {
    let pageInfo: PageInfo = {
      hasNextPage: true,
      endCursor: "",
    };

    while (pageInfo.hasNextPage) {
      const response: MergeRequestQueryData = await request(API_URL, QUERY, {
        milestone: milestone,
        labels: label,
        after: pageInfo.endCursor,
      });

      pageInfo = response.project.mergeRequests.pageInfo;

      response.project.mergeRequests.nodes.forEach((node) => {
        const mergeRequest: MergeRequest = {
          id: node.id,
          title: node.title,
          username: node.author.username,
          avatarUrl: node.author.avatarUrl.startsWith("http")
            ? node.author.avatarUrl
            : `https://gitlab.com${node.author.avatarUrl}`,
          milestone: node.milestone.title,
          url: node.webUrl,
          descriptionHtml: node.descriptionHtml,
          labels: node.labels.nodes.map((label) => label.title) || [],
        };

        mergeRequests.push(mergeRequest);
      });
    }
  }

  // de-duplicate merge requests
  return mergeRequests.reduce((accumulator: MergeRequest[], current) => {
    const existing = accumulator.find((el) => el.id === current.id);
    if (!existing) {
      accumulator.push(current);
    }
    return accumulator;
  }, []);
}
