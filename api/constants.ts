export const API_URL = "https://gitlab.com/api/graphql";
export const FROM_DATE = process.env.FROM_DATE || "2021-01-01";
export const OUTPUT_PATH =
  process.env.OUTPUT_PATH || "../frontend/public/data/papercuts";
export const LABELS = {
  "UI polish": "info",
  "Beautifying our UI": "success",
  "UX Paper Cuts": "tier",
};
