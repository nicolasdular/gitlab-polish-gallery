import { LABELS } from "./constants";
import { MergeRequest } from "./gitlab/merge_requests";
import { load } from "cheerio";

interface ImageSet {
  before: string;
  after: string;
}

export default class PaperCut {
  mergeRequestId: string;
  title: string;
  username: string;
  avatarUrl: string;
  url: string;
  milestone: string;
  images: ImageSet[];
  labels: string[];

  static fromMergeRequest(mergeRequest: MergeRequest): PaperCut | null {
    const images = getBeforeAndAfterImages(mergeRequest.descriptionHtml);

    if (images.length === 0) {
      return null;
    }

    return new PaperCut(mergeRequest, images);
  }

  private constructor(mergeRequest: MergeRequest, images: ImageSet[]) {
    this.mergeRequestId = mergeRequest.id;
    this.title = mergeRequest.title;
    this.avatarUrl = mergeRequest.avatarUrl;
    this.username = mergeRequest.username;
    this.url = mergeRequest.url;
    this.images = images;
    this.milestone = mergeRequest.milestone;
    this.labels = mergeRequest.labels.filter((label) =>
      Object.keys(LABELS).includes(label)
    );
  }
}

function getBeforeAndAfterImages(descriptionHtml: string): ImageSet[] {
  const doc = load(descriptionHtml);

  if (doc("table").length < 1) {
    return [];
  }

  const headers = getHeaders(doc);
  if (
    !headers.length ||
    !headers.includes("before") ||
    !headers.includes("after")
  ) {
    return [];
  }

  const imageSets: ImageSet[] = [];

  doc("tr").each((_, el) => {
    const row = doc(el);
    const imageSet = {};

    row.find("td").each((index, td) => {
      const src = getImageSrc(doc(td));
      if (!src) {
        return;
      }

      const header = headers[index];
      imageSet[header] = src;
    });

    if (Object.keys(imageSet).length) {
      imageSets.push(imageSet);
    }
  });

  return imageSets;
}

function getHeaders(doc) {
  return doc("th")
    .map((_, el) => doc(el).text().toLowerCase() as keyof ImageSet)
    .get();
}

function getImageSrc(td): string | null {
  const img = td.find("img");
  if (!img.length) return null;

  const src = img.attr("data-src");
  if (!src) return null;

  return src.startsWith("http") ? src : `https://gitlab.com${src}`;
}
