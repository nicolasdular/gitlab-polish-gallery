## Setup

- [Install bun](https://bun.sh/)

## Generate papercuts

```sh
bun run build
```

This will generate a `milestones.json` with all Milestones in `$OUTPUT_PATH` where `$OUTPUT_PATH` is by default `frontend/public/data/papercuts` (from the root folder of the repo).
For each milestone a `{milestoneTitle}.json` file gets generated in `$OUTPUT_PATH/papercuts`.

### Generate a limited set of papercuts

For development it's easier to generate only a limited set of papercuts. You can set a `FROM_DATE` which gets used to query only merge requests
after that date.

```sh
FROM_DATE='2024-01-01' bun run build
```
