# Papercuts

Hello 👋 this is a project to collect all the great small UI improvements we do at GitLab.
If you have questions, want to help (e.g. sponsor a nicer design for the page 😁) or have ideas how to improve, please create an issue or ping @nicolasdular.

[I WANT TO SEE THE GALLERY NOW](https://nicolasdular.gitlab.io/gitlab-polish-gallery/)

## How does it work?

All Merge Requests that have the label `UI polish` set and have before and after images in a table will show up here. Here is an example MR: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/53213.

You can use normal markdown tables, as long as you mark "Before" and "After" columns correctly. There is also no limit on the amount of rows.

## Development

### Getting started

- [Install bun](https://bun.sh/)
- [Install yarn](https://yarnpkg.com/)

```
cd api
bun install
FROM_DATE='2024-01-01' bun run build
cd ../frontend
yarn install
yarn run dev
open http://localhost:5173/
```
